﻿using System;
using Create_SendEmail.PageObjects;
using OpenQA.Selenium;

namespace Authorization.PageObjects
{
    public class AuthorizationPageObject
    {
        private IWebDriver _webDriver;

        private static string _nameFieldIdentificator = "login";
        private static string _passwordFieldIdentificator = "password";
        private static string _continueButtonIdentificator = "//button[@type='submit']";

        private readonly By _nameField = By.Name(_nameFieldIdentificator);
        private readonly By _passwordField = By.Name(_passwordFieldIdentificator);
        private readonly By _continueButton = By.XPath(_continueButtonIdentificator);

        public static string login = "test909090";
        public static string password = "vajpob-2wiSzy-mudsec";

        public AuthorizationPageObject(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public Create_SendEmailPageObject DoAuthorize()
        {
            _webDriver.FindElement(_nameField).SendKeys(login);
            _webDriver.FindElement(_passwordField).SendKeys(password);
            _webDriver.FindElement(_continueButton).Click();

            return new Create_SendEmailPageObject(_webDriver);
        }
    }

}

