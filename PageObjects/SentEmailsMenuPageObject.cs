﻿using System;
using OpenQA.Selenium;

namespace SendingEmail.PageObjects
{
	public class SentEmailsMenuPageObject
	{
        private IWebDriver _webDriver;

        private static string _sentEmailButtonIdentificator = "//span[text()='Отправленные']";
        private readonly By _sentEmailButton = By.XPath(_sentEmailButtonIdentificator);

        public SentEmailsMenuPageObject(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public void OpenSentTab()
        {
            _webDriver.FindElement(_sentEmailButton).Click();
        }
    }
}

