﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.DevTools;

namespace Create_SendEmail.PageObjects
{
	public class Create_SendEmailPageObject
	{
        private IWebDriver _webDriver;

        private static string _createEmailButtonIdentificator = "//button[text()='Написать письмо']";
        private static string _recipientFieldIdentificator = "toFieldInput";
        private static string _topicFieldIdentificator = "subject";
        private static string _textFieldIdentificator = "mce_0_ifr";
        private static string _sendEmailButtonIdentificator = "button[class='button primary send']";

        private readonly By _createEmailButton = By.XPath(_createEmailButtonIdentificator);
        private readonly By _recipientField = By.Name(_recipientFieldIdentificator);
        private readonly By _topicField = By.Name(_topicFieldIdentificator);
        private readonly By _textField = By.Id(_textFieldIdentificator);
        private readonly By _sendEmailButton = By.CssSelector(_sendEmailButtonIdentificator);

        public static string mailRecipient = "verochka2007@ukr.net";
        public static string mailTopic = "Test letter";
        public static string mailText = "Hello";

        public Create_SendEmailPageObject(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public void CreateEmail()
        {
            _webDriver.FindElement(_createEmailButton).Click();
        }

        public void FillIn()
        {
            _webDriver.FindElement(_recipientField).SendKeys(mailRecipient);
            _webDriver.FindElement(_topicField).SendKeys(mailTopic);
            _webDriver.FindElement(_textField).SendKeys(mailText);
        }

        public void SendEmail()
        {
            _webDriver.FindElement(_sendEmailButton).Click();
        }
    }
}

