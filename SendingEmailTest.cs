﻿using System;
using System.Threading;
using Authorization.PageObjects;
using Create_SendEmail.PageObjects;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace SendingEmailTest
{
    public class Test
    {
        private IWebDriver _webDriver;
        public static string mailboxLink = "https://accounts.ukr.net/login?client_id=9GLooZH9KjbBlWnuLkVX";

        [SetUp]
        public void Setup()
        {
            _webDriver = new ChromeDriver();
            _webDriver.Navigate().GoToUrl(mailboxLink);
            _webDriver.Manage().Window.Maximize();
        }

        [Test]
        public void SendingEmailTest()
        {
            var authorization = new AuthorizationPageObject(_webDriver);
            authorization.DoAuthorize();

            _webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            var sendEmail = new Create_SendEmailPageObject(_webDriver);
            sendEmail.CreateEmail();
            sendEmail.FillIn();
            _webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
            sendEmail.SendEmail();
        }

        [TearDown]
        public void TearDown()
        {
           _webDriver.Quit();
        }
    }
}
